#ifndef __TOKEN_ID__HPP_
#define __TOKEN_ID__HPP_

namespace lang
{
enum TokenID
{
	reserved=-1,	// class, import etc.
	iden=0,			// a-zA-Z0-9_$
	add=1,			// +
	sub=2,			// -
	mul=3,			// *
	div=4,			// /
	backq=5,		// `
	sgq=6,			// '
	dbq=7,			// "
	equal=8,		// =
	pow=9,			// ^^
	root=10,		// ..
	lrbrac=20,		// (
	rrbrac=21,		// )
	lsbrac=22,		// [
	rsbrac=23,		// ]
	lcbrac=24,		// {
	rcbrac=25,		// }
	lpbrac=26,		// <
	rpbrac=27,		// >
	lgc_and=32,		// &&
	lgc_or=33,		// ||
	lgc_not=34,		// !
	lgc_nand=35,	// !&&
	lgc_nor=36,		// !||
	lgc_xor=37,		// ^^
	lgc_xnor=38,	// !^^
	band=64,		// &
	bor=65,			// |
	bnot=66,		// ~
	bnand=67,		// !&
	bnor=68,		// !|
	bxor=69,		// ^
	bxnor=70,		// !^
	scmt=125,		// # some single-line comment
	lmcmt=126,
	rmcmt=127		/* #[
		some multi-
		line 
		comment
	]# */
};
}

#else
#warning You re-included this file!
#endif
