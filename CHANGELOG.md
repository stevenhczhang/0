### v0.0.0.22
Issue #3.

### v0.0.0.21
Another patch.

### v0.0.0.20
Another patch for #1.

### v0.0.0.19
Fixed issue #1, preparing for pull request #2.

### v0.0.0.15 ~ v0.0.0.18
Update .gitlab-ci.yml.

### v0.0.0.14
Beuatified the output of the compiler.

### v0.0.0.13
added tag-commit-ratio.sh to remind myself about the tag-commit ratio.

### v0.0.0.12
Added support for non-existing files that may cause a segmentation fault bug. (\#0100004)

### v0.0.0.11
More stable, added fatal error when no file is specified.

### v0.0.0.10
Fixed some warnings. (\#0100003)

### v0.0.0.9
Continue implementing ParseOption.cpp

### v0.0.0.8
Added lexer/Tokenizer/Tokenizer.hpp

### v0.0.0.7
Added lexer/TokenID/TokenID.hpp

### v0.0.0.6
Added stub for lexer/lexer.hpp

### v0.0.0.5
Added CHANGELOG.md and updated bug. (\#0100002)
ParseResult pr(); -> ParseResult pr; To avoid segmentation fault and some system-level errors.

### v0.0.0.4
Added parts of support for -h, -W, -E (\#0180001)

### v0.0.0.3
Fixed some `illegal hardware bug' (\#0100001)

### v0.0.0.2
Second release, nothing important

### v0.0.0.1
First release. (\#0100000, initial number)
