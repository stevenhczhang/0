#include "Helper.hpp"  

namespace lang{
Helper::Helper(string progn)
{
    progname=progn;
    this->h_msg=\
R"(
Usage:  <file...> [options]
Note: 1) This grammer is mandatory, anything not fitting this grammer, will result in runtime error, 
         or segmantation fault.
      2) <file...> is omittable IF AND ONLY IF options CONTAINS AND ONLY CONTAINS `-h' or `-v'.
)";
}

Helper::~Helper()
{
    //just explicitly declaring this de-constructor. Actually does nothing.
}

void Helper::print()
{
    string _h_msg(h_msg);
    _h_msg.insert(8,progname);
    printf("%s\n",_h_msg.c_str());
}
}
