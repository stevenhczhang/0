#ifndef _HELPER_HPP_
#define _HELPER_HPP_

#include<string>
#include<cstdio>
using std::string;
using std::printf;

namespace lang{
class Helper
{
	private:
	string progname;
	string h_msg;

	public:
		explicit Helper(string progn);
		virtual ~Helper();
		virtual void print();
};

} // namespace lang

#else
#warning You re-including this file!
#endif
