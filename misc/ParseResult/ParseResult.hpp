#ifndef _PARSERESULT_HPP_
#define _PARSERESULT_HPP_
#include<utility>
#include<vector>
#include<cstdio>
using namespace std;
namespace lang
{
/**
 * @brief flag type
 * Only the following values are ok:
 * -1(for ONLY warning), 0(for none), 1(for ONLY error), 2(for warning AND error)
 */
typedef int8_t flag;
/**
 * @brief standard type.
 * Only the following values are ok:
 * 20(for 2020)
 */
typedef int8_t std_t;
/**
 * @brief error type(a.k.a bool)
 * 0 for none or -Wno-error, 1 for -Werror
 */
typedef bool error_t;
class ParseResult
{
	private:
		// Note: e_* means only in -Wextra or -Eextra
		// and m_* means only on -Weven-more
		flag all;
		flag extra;
		flag return_type;
		flag e_signed_comp;
		flag e_dsize_comp;
		flag dsize_conv;
		flag nreturn; // no `return statement in non-void method'
		error_t err;
		std_t std;
		vector<FILE*> files;
	public:
		vector<FILE*> getfilep();
		explicit ParseResult();
		virtual ~ParseResult();
		void pushback_file(FILE*);
		bool empty_file();
		FILE* at_file(int);
};

}
#else
#warning You re-included this file!
#endif
