#include "ParseResult.hpp"  
#include<cstdio>
using std::FILE;

namespace lang
{
ParseResult::ParseResult(){}
ParseResult::~ParseResult(){}
vector<FILE*> ParseResult::getfilep(){return files;}
void ParseResult::pushback_file(FILE* file){files.push_back(file);}
FILE* ParseResult::at_file(int i){return files.at(i);}
bool ParseResult::empty_file(){return files.empty();}
}
