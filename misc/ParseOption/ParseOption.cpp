#include "ParseOption.hpp"
#include "../Helper/Helper.hpp"
#include<stdexcept>
using lang::Helper;
using std::__throw_runtime_error;
using std::runtime_error;

namespace lang{
[[noreturn]] inline void __throw_unknown_option_error(string s){__throw_runtime_error(string("Unknown option: "+s).c_str());}
ParseOption::ParseOption(queue<string> q1)
{
    while(!q1.empty())
    {
        q.push(q1.front());
        q1.pop();
    }
}

ParseOption::~ParseOption()
{
    while(!q.empty())q.pop();
}

ParseResult ParseOption::parse()
{
    ParseResult pr;
    queue<string> q2(q);
    string prognam=q2.front();
    Helper helper(prognam);
    q2.pop();
    while(!q2.empty() && q2.front().at(0)!='-')
    {
        FILE* file=fopen(q2.front().c_str(),"r");
        if(!file)
        {
            fprintf(stderr,"%s: \033[33m\033[1m\033[4mwarning\033[0m: \033[37m%s: %s\033[0m\n",prognam.c_str(),q2.front().c_str(),"no such file or directory, ignoring");
            q2.pop();
            continue;
        }
        pr.pushback_file(file);
        q2.pop();
    }
    string second=q2.front().substr(0,2);
    // printf("%s\n",second.c_str());
    try{if(pr.empty_file() &&second!="-h" && second!="-v")__throw_runtime_error("no file(s) specified.");}
    catch(runtime_error &e)
    {
        string wht=e.what();
        fprintf(stderr,"%s: \033[31m\033[1m\033[4mfatal error\033[0m: \033[37m%s\033[0m\n",prognam.c_str(),wht.c_str());
    }
    while(!q2.empty())
    {
        const string s=q2.front();
        q2.pop();
        if(s[1]==s[0]&&s[1]=='-')
        {
            break;
        }
        try{
        if(s[0=='-'])
        {
            switch(s[1])
            {
                case 'h':
                    if(s!="-h" && s!="-help")__throw_unknown_option_error(s);
                    helper.print();
                    break;
                case 'W':
                    if(s.length()==2)__throw_runtime_error("Missing parameter for -W");
                    break;
                case 'E':
                    if(s.length()==2)__throw_runtime_error("Missing parameter for -E");
                    break;
                default: __throw_unknown_option_error(s);
            }
        }
        }
        catch(runtime_error& e)
        {
            fprintf(stderr,"%s: \033[31m\033[1m\033[4merror\033[0m: \033[37m%s\033[0m\n",prognam.c_str(),e.what());
            exit(3);
        }
    }
    return pr;
}

} // namespace lang

