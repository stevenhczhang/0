#ifndef _PARSE_OPTION_HPP_
#define _PARSE_OPTION_HPP_

#include<queue>
#include<string>
using std::string;
using std::queue;
#include "../ParseResult/ParseResult.hpp"
using lang::ParseResult;

namespace lang
{
class ParseOption
{
	private:
		queue<string> q;
	public:
		explicit ParseOption(queue<string> q1);
		virtual ~ParseOption();
		virtual ParseResult parse();
};
} // namespace lang

#else
#warning You Re-included this file!
#endif
