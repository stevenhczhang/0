export __a=$(git tag |sed '/^$/d'| awk '{print NR}'| sort -nr| sed -n '1p')
export __b=$(git rev-list --all --count)
export __c=$(printf '%.6f\n' $(echo "$__a/$__b" | bc -l))
export __d=$(printf '%.0f\n' $(echo "$__c*1000000" | bc -l))

if  test $__d -lt 750000
then
  echo "Your tag-commit ratio ($__a/$__b = $__c < 0.750000) is too low!"
else
  if test $__d -gt 1000000
  then
    echo "Your tag-commit ratio ($__a/$__b = $__c > 1.000000) is too high!"
  else
    echo "Your tag-commit ratio ($__a/$__b = $__c) is just right!"
  fi
fi
